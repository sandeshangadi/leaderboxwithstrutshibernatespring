package com.leaderbox.dao;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.leaderbox.bean.Category;

public class CategoryDaoImpl extends HibernateDaoSupport 
implements CategoryDao {
	
	public boolean insertCategory(Category category) {	
		
		try {
			 getHibernateTemplate().save(category);
		} catch (DataAccessException dae) {
			System.out.println("Exception occured while saving the category : " + dae.getMessage());
			return false;
		}
		
		
		return true;
	}
	
	public boolean updateCategory(Category category) { 
		
		try {
			getHibernateTemplate().saveOrUpdate(category);
		} catch (DataAccessException dae) {
			System.out.println("Exception occured while saving the category : " + dae.getMessage());
			return false;
		}
		
		return true;
	}
	
	public List<Category> listAllCategory() throws SQLException {
		return getHibernateTemplate().find("from Category");
	}
	
	public Category getCategory(int id)throws SQLException{
		return (Category) getHibernateTemplate().get(Category.class,id);
	}
	
	public void deleteCategory(int id)throws SQLException{
		 Category category = (Category) getHibernateTemplate().get(Category.class,id);
		 getHibernateTemplate().delete(category);
	}

}
