package com.leaderbox.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.leaderbox.bean.Score;



public class ScoreDao {private String jdbcURL;
private String jdbcUsername;
private String jdbcPassword;
private Connection jdbcConnection;

public ScoreDao(String jdbcURL, String jdbcUsername, String jdbcPassword) {		
	this.jdbcURL = jdbcURL;
	this.jdbcUsername = jdbcUsername;
	this.jdbcPassword = jdbcPassword;

}
  protected void connect() throws SQLException {
        if (jdbcConnection == null || jdbcConnection.isClosed()) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new SQLException(e);
            }
            jdbcConnection = DriverManager.getConnection(
                                        jdbcURL, jdbcUsername, jdbcPassword);
        }
    }
  protected void disconnect() throws SQLException {
        if (jdbcConnection != null && !jdbcConnection.isClosed()) {
            jdbcConnection.close();
        }
    }
  public boolean insertScore(Score score) throws SQLException {
        String sql = "INSERT INTO score (Cat_id, name,score) VALUES (?, ?, ?)";
        connect();
         
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setInt(1, score.getCatId());
        statement.setString(2, score.getName());
        statement.setInt(3, score.getScoreNo());	       
         
        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowInserted;
    }
  
	
	
	 
  
  public List<Score> listAllScore(int id) throws SQLException {
        List<Score> listScore = new ArrayList<>();
         
        String sql = "SELECT * FROM score WHERE Cat_id = ? ORDER BY score DESC";
       
        connect();
         
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery(sql);
         
        while (resultSet.next()) {
        	String name = resultSet.getString("name");
            int scoreNo = Integer.valueOf(resultSet.getString("score"));
            
             
            Score score = new Score(id,scoreNo, name );
            listScore.add(score);
        }
         
        resultSet.close();
        statement.close();
         
        disconnect();
         
        return listScore;
    }

	

}
