package com.leaderbox.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.leaderbox.util.DBConnect;
import com.leaderbox.bean.Category;



public interface CategoryDao {
	
	boolean insertCategory(Category category);
	boolean updateCategory(Category category);
	List<Category> listAllCategory() throws SQLException;
	Category getCategory(int id)throws SQLException;
	void deleteCategory(int id)throws SQLException;
	/*
	 * public boolean insertCategory(Category category) throws SQLException { String
	 * sql = "INSERT INTO category (Category_title, Category_desc) VALUES (?, ?)";
	 * // connect();
	 * 
	 * PreparedStatement statement =
	 * DBConnect.getConnection().prepareStatement(sql); statement.setString(1,
	 * category.getCategoryTitle()); statement.setString(2,
	 * category.getCategoryDesc());
	 * 
	 * 
	 * boolean rowInserted = statement.executeUpdate() > 0; statement.close(); //
	 * disconnect(); return rowInserted; }
	 * 
	 * public List<Category> listAllCategory() throws SQLException { List<Category>
	 * listCategory = new ArrayList<>(); String sql = "SELECT * FROM category";
	 * ResultSet resultSet =
	 * DBConnect.getConnection().createStatement().executeQuery(sql);
	 * 
	 * while (resultSet.next()) { int id = resultSet.getInt("Category_id"); String
	 * title = resultSet.getString("Category_title"); String desc =
	 * resultSet.getString("Category_desc"); Category cat = new
	 * Category(id,desc,title); listCategory.add(cat); }
	 * 
	 * resultSet.close();
	 * 
	 * //disconnect();
	 * 
	 * return listCategory; }
	 * 
	 * public boolean deleteCategory(int id) throws SQLException { String sql =
	 * "DELETE FROM category where Category_id = ?";
	 * 
	 * // connect();
	 * 
	 * PreparedStatement statement =
	 * DBConnect.getConnection().prepareStatement(sql); statement.setInt(1,id);
	 * 
	 * boolean rowDeleted = statement.executeUpdate() > 0; statement.close(); //
	 * disconnect(); return rowDeleted; }
	 * 
	 * public boolean updateCategory(Category category) throws SQLException { String
	 * sql = "UPDATE category SET Category_title = ?, Category_desc = ?"; sql +=
	 * " WHERE Category_id = ?"; //connect();
	 * 
	 * PreparedStatement statement =
	 * DBConnect.getConnection().prepareStatement(sql); statement.setString(1,
	 * category.getCategoryTitle()); statement.setString(2,
	 * category.getCategoryDesc()); statement.setInt(3, category.getId());
	 * 
	 * boolean rowUpdated = statement.executeUpdate() > 0; statement.close(); //
	 * disconnect(); return rowUpdated; }
	 * 
	 * public Category getCategory(int id) throws SQLException { Category cat =
	 * null; String sql = "SELECT * FROM category WHERE Category_id = ?";
	 * 
	 * //connect();
	 * 
	 * PreparedStatement statement =
	 * DBConnect.getConnection().prepareStatement(sql); statement.setInt(1, id);
	 * 
	 * ResultSet resultSet = statement.executeQuery();
	 * 
	 * if (resultSet.next()) {
	 * 
	 * String title = resultSet.getString("Category_title"); String desc =
	 * resultSet.getString("Category_desc");
	 * 
	 * 
	 * cat = new Category(id, desc, title); }
	 * 
	 * resultSet.close(); statement.close();
	 * 
	 * return cat; }
	 */


}
