package com.leaderbox.bo;

import java.sql.SQLException;
import java.util.List;

import com.leaderbox.bean.Category;

public interface CategoryBo {
	boolean insertCategory(Category category);
	boolean updateCategory(Category category);
	void deleteCategory(int id)throws SQLException;
	List<Category> listAllCategory() throws SQLException;
	Category getCategory(int id)throws SQLException;
	
	/*
	 * boolean deleteCategory(int id); boolean updateCategory(Category category)
	 * throws SQLException; Category getCategory(int id) throws SQLException;
	 */

}
