package com.leaderbox.bo;

import java.sql.SQLException;
import java.util.List;

import com.leaderbox.dao.CategoryDao;
import com.leaderbox.bean.Category;

public class CategoryBoImpl implements CategoryBo {
	CategoryDao categoryDao;
	

	public void setCategoryDao(CategoryDao categoryDao) {
		this.categoryDao = categoryDao;
	}
	
	 public boolean insertCategory(Category category){
		return categoryDao.insertCategory(category);
		
    }
	 
	 public boolean updateCategory(Category category){
		 return categoryDao.updateCategory(category);
		
    }
    
    //call DAO to return customers
	 public List<Category> listAllCategory() throws SQLException{
        return categoryDao.listAllCategory();
    }
	 
	 
	 
	 public Category getCategory(int id) throws SQLException{
	        return categoryDao.getCategory(id);
	    }
	 
	 public void deleteCategory(int id) throws SQLException{
	        categoryDao.deleteCategory(id);
	    }
	 
	/*
	 * public boolean deleteCategory(int id) { try { return
	 * categoryDao.deleteCategory(id); } catch (SQLException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } return false; }
	 * 
	 * public boolean updateCategory(Category category) throws SQLException { return
	 * categoryDao.updateCategory(category);
	 * 
	 * }
	 * 
	 * public Category getCategory(int id) throws SQLException { return
	 * categoryDao.getCategory(id); }
	 */
	 
}
