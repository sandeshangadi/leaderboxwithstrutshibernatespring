package com.leaderbox.bean;

import java.io.Serializable;

public class Category implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String categoryDesc;
	private String categoryTitle;
	
	 public Category(){
			
		}
	 
	 public Category(int id) {
	    	
	    	this.id = id;
	   	}
	    
		public Category(int id, String categoryDesc, String categoryTitle) {
			super();
			this.id = id;
			this.categoryDesc = categoryDesc;
			this.categoryTitle = categoryTitle;
		}
		
		public Category(String categoryDesc, String categoryTitle) {
			super();
			this.categoryDesc = categoryDesc;
			this.categoryTitle = categoryTitle;
		}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategoryDesc() {
		return categoryDesc;
	}

	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}

	public String getCategoryTitle() {
		return categoryTitle;
	}

	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", categoryDesc=" + categoryDesc + ", categoryTitle=" + categoryTitle + "]";
	}

	
    
   
    
    
	

}
