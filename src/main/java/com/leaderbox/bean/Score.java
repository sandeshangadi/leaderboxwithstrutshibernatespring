package com.leaderbox.bean;

public class Score {
	private int scoreId;
	private int catId;
	private int scoreNo;
	private String name;
	public Score(int catId) {
		super();
		this.catId = catId;
	}
	public Score( int catId, int scoreNo, String name) {
		super();		
		this.catId = catId;
		this.scoreNo = scoreNo;
		this.name = name;
	}
	public int getScoreId() {
		return scoreId;
	}
	public void setScoreId(int scoreId) {
		this.scoreId = scoreId;
	}
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public int getScoreNo() {
		return scoreNo;
	}
	public void setScoreNo(int scoreNo) {
		this.scoreNo = scoreNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
