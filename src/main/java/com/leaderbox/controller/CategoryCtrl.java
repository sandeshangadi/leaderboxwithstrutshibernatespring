package com.leaderbox.controller;


import java.util.List;

import com.leaderbox.bo.CategoryBo;
import com.leaderbox.bean.Category;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/*import javax.servlet.http.HttpServletRequest;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.Leaderbox.dao.CategoryDao;
import org.apache.struts2.ServletActionContext;
import java.sql.SQLException;*/

public class CategoryCtrl extends ActionSupport implements ModelDriven{
	
	/**extends ActionSupport
	 * 
	 */
	//private static final long serialVersionUID = 1L;
	Category cat = new Category();		
	private List<Category> categoryList;
	private String sm = ""; 
	private String em = "";
	public String getSm() {
		return sm;
	}

	public void setSm(String sm) {
		this.sm = sm;
	}

	public String getEm() {
		return em;
	}

	public void setEm(String em) {
		this.em = em;
	}

	CategoryBo categoryBo;

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}
	
	public Object getModel() {
        return cat;
    }

	public Category getCat() {
		return cat;
	}

	public void setCat(Category cat) {
		this.cat = cat;
	}

	public CategoryBo getCategoryBo() {
		return categoryBo;
	}

	public void setCategoryBo(CategoryBo categoryBo) {
		this.categoryBo = categoryBo;
	}
	
	 public String saveCategory() throws Exception{	        
	       
		 boolean status = categoryBo.insertCategory(cat);	        
		 if(status) {
			 setSm("Category Saved Successfully");
			 return "SUCCESS"; 
			 }
		 else {
			 setEm("Category Not Saved");
			 return "SUCCESS"; 
			 }
	    
	    }
	 
	 public String updateCategory() throws Exception{
	        
	       
		 boolean status = categoryBo.updateCategory(cat);	     
		 if(status) {
			 setSm("Category Updated Successfully");
			 return "SUCCESS"; 
			 }
		 else {
			 setEm("Category Not Updated");
			 return "SUCCESS"; 
			 }
	    
	    }
	    
	    //list all customers
	    public String getAllCategory() throws Exception{
	        
	    	categoryList = categoryBo.listAllCategory();	        
	        return "SUCCESS";
	        
	    }
	    
 public String editCategory() throws Exception{
	
	 Category category = categoryBo.getCategory(cat.getId()); 
	  cat.setId(category.getId());	 
	  cat.setCategoryTitle(category.getCategoryTitle());
	  cat.setCategoryDesc(category.getCategoryDesc()); 
	  
	  return "SUCCESS"; 
	        
	    }
 
 public String deleteCategory() throws Exception {
	 
	 categoryBo.deleteCategory(cat.getId());
	 getAllCategory();
	  return "SUCCESS"; 
	  }
	
	/*
	 * private String sm = ""; private String em = ""; public Category getCat() {
	 * return cat; } public void setCat(Category cat) { this.cat = cat; } public
	 * CategoryDao getCatDAO() { return catDAO; } public void setCatDAO(CategoryDao
	 * catDAO) { this.catDAO = catDAO; } public List<Category> getCategoryList() {
	 * return categoryList; } public void setCategoryList(List<Category>
	 * categoryList) { this.categoryList = categoryList; } public String getSm() {
	 * return sm; } public void setSm(String sm) { this.sm = sm; } public String
	 * getEm() { return em; } public void setEm(String em) { this.em = em; }
	 * 
	 * public String saveCategory() throws SQLException {
	 * 
	 * cat.setCategoryTitle(cat.getCategoryTitle());
	 * cat.setCategoryDesc(cat.getCategoryDesc()); boolean status =
	 * catDAO.insertCategory(cat); if(status) {
	 * setSm("Category Saved Successfully"); return "SUCCESS"; } else {
	 * setEm("Category Not Saved"); return "SUCCESS"; } }
	 * 
	 * public String updateCategory() throws SQLException {
	 * 
	 * cat.setId(cat.getId()); cat.setCategoryTitle(cat.getCategoryTitle());
	 * cat.setCategoryDesc(cat.getCategoryDesc()); boolean status =
	 * catDAO.updateCategory(cat); if(status) {
	 * setSm("Category Updated Successfully"); return "SUCCESS"; } else {
	 * setEm("Category Not Saved"); return "SUCCESS"; } }
	 * 
	 * public String deleteCategory() throws SQLException {
	 * 
	 * HttpServletRequest request = (HttpServletRequest)
	 * ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST); int id =
	 * Integer.parseInt(request.getParameter("id")); boolean
	 * status=catDAO.deleteCategory(id); if(status) {
	 * setSm("Category deleted Successfully"); getAllCategory(); return "SUCCESS"; }
	 * else { setEm("Category Not deleted"); return "SUCCESS"; } }
	 * 
	 * public String getAllCategory() throws SQLException { categoryList =
	 * catDAO.listAllCategory(); return "SUCCESS";
	 * 
	 * }
	 * 
	 * public String editCategory() throws SQLException { HttpServletRequest request
	 * = (HttpServletRequest)
	 * ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST); int id =
	 * Integer.parseInt(request.getParameter("id")); Category category
	 * =catDAO.getCategory(id); cat.setId(category.getId());
	 * cat.setCategoryTitle(category.getCategoryTitle());
	 * cat.setCategoryDesc(category.getCategoryDesc()); return "SUCCESS"; }
	 */


}
