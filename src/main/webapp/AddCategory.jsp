<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="s" uri="/struts-tags"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add category</title>
<%@include file="COMPONENTS/common_css_js.jsp" %>
	
	<script>
	$(document).ready(function() {	
		
		/*  $( '#addCategory' ).on( 'click', function() {
			 $('#leaderboxScection').load('AddEditCategory.jsp');	           
	      }); 		 */
			
	
	});
</script>
</head>
<body>

 <div class="container-fluid">
  <h1 style="text-align:center;">Score LeaderBox</h1>
  <h1 id="hdCat" style="text-align:center;">Add Category</h1>
  <div class="row" style="height:100">
    <div class="col-xl-2">
    <%@include file="COMPONENTS/commonMenu.jsp" %>
  </div>
    <div id="leaderboxScection" class="col-xl-10" style="background-color:lavenderblush;">
    <div class="container">
<s:if test="sm!=null">
<s:property value="sm"/> 
</s:if>
<s:if test="em!=null">
<s:property value="em"/>
</s:if>
							<s:form action="/Category/saveCategory" method="post">										
							<s:textfield  name="cat.categoryTitle" label="CategoryName" />
							<s:textfield  name="cat.categoryDesc" label="Description" />			
							<s:submit value="Save"></s:submit>							
							</s:form>
								<a href="/Leaderbox/Category/CategoryList.action">Go To List Page</a>
									
</div>
    </div>
  </div>
</div>


</body>
</html>