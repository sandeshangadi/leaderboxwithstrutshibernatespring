<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Hello World Java EE</title>
<%@include file="COMPONENTS/common_css_js.jsp" %>
	<style>
.categoryCrud {
	text-align: center;
}

.scoreAdd {
	text-align: center;
}
.leaderbox {
	text-align: center;
}
a {
    color: #00f;
}
</style>
<script>
	$(document).ready(function() {	
		if($( 'ul.list-group li' ).attr("value")=='CATEGORY')
			{
			   $('#leaderboxScection').load('CategoryList.jsp');
			}
		 $( 'ul.list-group li' ).on( 'click', function() {
			  $( this ).parent().find( 'li.active' ).removeClass( 'active' );
	            $( this ).addClass( 'active' );	
	            var a = $(this).attr("value");
	            switch (a) {
	            case 'CATEGORY':
	            	$('#leaderboxScection').load('CategoryList.jsp');
	              break;
	            case 'SCORE':
	            	$('#leaderboxScection').load('AddScore.jsp');
	              break;
	            case 'LEADERBOX':
	            	$('#leaderboxScection').load('CategoryLeaderBox.jsp');
		         break;
	          }
	      }); 
		
	});
</script>
</head>
<body>
    <div class="container-fluid">
  <h1 style="text-align:center;">Score LeaderBox</h1>
  
  <div class="row" style="height:100">
    <div class="col-xl-2">
    <ul class="list-group">
    <li class="list-group-item active " value="CATEGORY"><a class="categoryCrud" href="#">CATEGORY</a></li>				
    <li class="list-group-item" value="SCORE"><a class="scoreAdd" href="#">SCORE</a></li>
    <li class="list-group-item" value="LEADERBOX"><a class="leaderbox" href="#">LEADERBOX</a></li>
  </ul>
  </div>
    <div id="leaderboxScection" class="col-xl-10" style="background-color:lavenderblush;">
    </div>
  </div>
</div>
</body>
</html>