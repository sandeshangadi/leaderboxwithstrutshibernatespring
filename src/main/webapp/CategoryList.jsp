<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="s" uri="/struts-tags"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Category List</title>
<%@include file="COMPONENTS/common_css_js.jsp" %>
<style type="text/css">
table{
border-collapse:collapse;
}
th,td{padding:10px;}
tfoot{text-align:center;}
</style>
	<script>
	$(document).ready(function() {	
		
		 $( '#addCategory' ).on( 'click', function() {
			 $('#leaderboxScection').load('AddEditCategory.jsp');	           
	      }); 		
			
	
	});
</script>
</head>
<body>
<div class="container-fluid">
  <h1 style="text-align:center;">Score LeaderBox</h1>
  
  <div class="row" style="height:100">
    <div class="col-xl-2">
    <%@include file="COMPONENTS/commonMenu.jsp" %>
  </div>
    <div id="leaderboxScection" class="col-xl-10" style="background-color:lavenderblush;">
<div class="container text-left">
					<a href="/Leaderbox/Category/AddCategory.action"  class="btn btn-success"  >Add New Category</a>						
</div>
<br>
				<div class="container">
					<h3 class="text-center">List of Category</h3>
					<table >
						<thead>
							<tr>
							    <th>ID</th>
								<th>Title</th>
								<th>Description</th>
								<th colspan="2">Actions</th>
							</tr>
						</thead>
						<tbody>
							<s:iterator value="categoryList">
								<tr>
									<td><s:property value="id"/></td>
									<td><s:property value="categoryTitle"/></td>
									<td><s:property value="categoryDesc"/></td>
									<td><a href="/Leaderbox/Category/EditCategory.action?id=<s:property value="id"/>">Edit</a></td>
									<td><a onclick="return confirm('Are you sure to delete category?')" href="/Leaderbox/Category/deleteCategory.action?id=<s:property value="id"/>">Delete</a></td>
								
									
								</tr>
							</s:iterator>

						</tbody>

					</table>
				</div>
				</div>
 </div>
 </div>
</body>
</html>